This is the presentation William gave at FOSDEM in December 2020

You can find more information [on the wiki for this repository][repowiki]

To build it, you need backslide, which is an npm module:

    $ npm install -g backslide

To build PDFs, do:

    $ bs pdf --verbose -- --chrome-arg=--no-sandbox

To build HTML do:

    $ bs e

To run a local server to play with the presentation do:

    $ bs s

The presentation itself is in `presentation.md` and is in _remark.js_.

You can find more about remark at: <https://github.com/gnab/remark/wiki>

[repowiki]: https://gitlab.com/celduin/media/bazelcon-dec-2019/wikis/home

