title: Deploy To Hardware
class: animation-fade
layout: true

<!-- This slide serves as the base layout for all the slides -->
.bottom-bar[
  <img height="20px" width="20px" src="assets/codethink-o-white.svg" />
  {{title}}
]

---
class: impact

# {{title}}
## William Salmon<br>`will.salmon@codethink.co.uk`

???

* Placeholder slide
* Here to act as timer-start
* Advance to next (identical slide) to begin timer.

---
count: false
class: impact

# {{title}}
## William Salmon<br>`will.salmon@codethink.co.uk`

???

Say "Hi and welcome"

* Embedded context and that you will talk about CI

---
class: playful

# Who Am I?

???

* William Salmon

* Aerospace
* Codethink


* Codethink
    * many industries 
    * many projects

---
class: playful

# What have I been working on

???

* Embedded Systems
    * Microcontrollers
    * Integration
    * Deployment

* Lots of opensource

---

# Celduin

The key parts for CRASH
* Promote open tools
* Promote Reproducible & [trustable](https://www.trustablesoftware.com/) workflows
* Fast builds

???

---

# CRASH


.col-6[
<img height="400px" src="assets/Jetbot.png"/>
]

.col-6[
### Celduin Robotic Automation Showcase

[Hosted on gitlab.com](http://gitlab.com/celduin/crash)

]

???

Celduin Robotic Automation Showcase

Fun App
  * But representative of many real products

Jetbot
  * Nvidia publish everything to make your own
  * Common platform

CSI -- Camera Serial Interface
GPU -- 128-core Maxwell
IO -- GPIO -- I2C
configuration  -- WIFI


---

# Systems integration

.col-6[
<img height="400px" src="assets/Jetbot.png"/>
]

???

For the purpose of this talk!

* Software
* Assets - ML models
* Drivers
* Kernel
* Configuration

---
# Testing and Deployment

* Try to test what we are going to deploy as early as possible
* Trade off between feedback time and mirroring prod
    * Docker does a good job of this but is not suitable for our usecase
    * For embedded systems we can only get so close due to the importance of hardware
        * Sensors
        * Projects like those talked about earlier are very important for CI
        * But allowing devs to test on desk realistically is very valuable

???

Trade offs
Developer Experience

* Not always but often we use Docker images,
* We can install our software as a separate layer on top of a base
* Then the test server or staging or prod can all use the same layer 
* If they already have other layers they only need to fetch the new layer!

---
# Systems integration

Tools:
* Package
    * Apt
    * Dnf
* Layered
    * Docker
* Full "disk image"/partition
    * Buildroot
    * Yocto
    * Buildstream
???

Repeat why Package and Docker are no good

* Testing
* Reproducible

* Types of artifacts

testable -> CI
* Test the whole system
Cacheable

---
# CI / CD with Hardware

What tools let us:

* Continuously integrate with hardware

* Continuously deploy with hardware

In a way that is Testable and Reproducible

"A/B" Systems could reduce the burden on Deploy and Test

???

A/B is really important

NO PACKAGES PLEASE

What did the last few slide mean for these questions?

---

# Deployment

Tool options

* Mender
* Lava
* OSTree
* Aktualizr

???

Lots of people know Mender

Recently added new futures

Aktualizr -- AGL, Automotive Grade Linux use this and it is well integrated 

---

# OSTree

* OSTree uses a Content Addressable Store so it only needs to download what has changed
* OSTree has commits, a little like docker layers

* It does not do on-board integration like apt/dnf, it just deploys the changed files!

???

You do have to reintegrate for every deployment configuration unlike apt/dnf.

---
# Examples

* Flatpack
* Fedora Atomic/Silverblue
* Aktualizr -- Uptane

???

Flatpack has proven OSTree as a really good delivery mechanism
Atomic has taken the concept to the OS level

We wanted to see how well this worked in the embedded level, we're not really doing anything new, but we are putting it together in a "embedded" centric way.


---
# OSTree on embedded

* Almost everything can be inside the OSTree deployment

* Diff based updates -> very fast updates -> lends itself to the developer workflow

* The same Docker like workflow of using the same OSTree commit / image for test and deploy

  * OSTree brings this to places where you care about which Docker ignores, kernel, drivers, etc

* When we do "incremental" updates, we have already integrated
   
  * OSTree increments do not need locally integrating

* Can be integrated with uboot to give A/B updates with fall back if kernel or userland do not run/work


???

Docker can be used for embedded, there is a big push for that but you still need to be able to update drivers and kernel etc.

Mender can do diff updates but not with free functionality last time checked I checked.

---
# Building our OSTree commits

Integration tool can create the commits

But a dev can checkout a commit/deployment, tweak commit and push to a local test

This is much better than just tweaking with a file system as the commit knows all the files they changed

Even if the dev used a integration tool, they don't need to re-flash the whole thing.

---
# Dev Commit

Our team used OSTree in its developer workflow early on and found it very useful.

With A/B updates if you upload a broken system, then your build can fall back to the last state.

???

We are still trying to develop this, and doing it in the open with the tools is a little tricky

So we have shown how the dev side of this is nice and can mirror the CI side
What does the CI side look like?

---
# Multi Pipeline


1. Support:
  * Docker image creation
  * Infra repo

2. Components:
  * ML model training data
  * ML model store
  * App code

3. Integration:
  * Integration configuration, a bst/Yocto project
      * App tests in intergration tool
      * Auto deployed on merge to master

???

This is not reliant on any one CI this can be done with any.

Upstream have lots of CI

---

# Integration Pipeline

.col-8[
<img height="400px" src="assets/pipeline2.png"/>
]

.col-4[

### Intergration Pipeline for master


Key steps:

* Build

* Test

* Push to OSTree

* Push disk image



]

???

* This pipeline is part of our POC, it could be optimised further

---
# Building this setup

https://gitlab.com/celduin/crash

Most of the CI uses Gitlab but could use Github+CI or similar
* Docker image registry
* Gitlab runner with docker-machine driver creates
    * GPU runners
    * Arm runners
    * Multi-sized and privileged x86

???

Nothing is Gitlab specific

I patched docker-machine's AWS driver so we could use better storage/EBS

---
# What does Gitlab not have?

* Note: Different trade-offs for different git servers/CI setups

* Controlled Artifacts
    * All our code/elements are open source but our sources and imports from hardware vendors are not

* Runner Bastion

* Ostree server
    * Git pages

* Build Cache


???

All managed with Ansible, this is mostly public

Not triggered by CI but should be!

---
# Our solution for an OSTree server

Our POC

* Docker running nginx

* Docker with rsync

* This will only scale so well! But as almost everything is immutable it caches VERY well
    * Almost all of the OSTree content is a `Content Addressable Store`

* Scaling up is clearly possible as Flatpack has demonstrated.

---
# Notes / Summary

I am pleased with our software and its stack

I am pleased with how we build our software

I am pleased with how we integrate our software

I am please with our OTA infra as a POC but would not like to make any promises about it.

???

The OTA servers are the weakest part of the open source offering and the most pay-walled

---
# Looking forward

* Cache

* REAPI -- Remote Execution API

* OSTree tooling and server
    * Currently there are some implementations but they are very specific
    * The generic tooling is powerful but requires a lot of boilerplate

???

* Learn lessons form Flatpack and publish how to

There docs tend to be how to consume their OSTree rather than how to do it yourself



---
# Any Questions?









